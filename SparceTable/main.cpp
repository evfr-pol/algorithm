#include <cmath>
#include <iostream>
#include <vector>

template <typename T>
class SparseTable {
 public:
  struct ElemOfSparce {
    ElemOfSparce(const T& result, int64_t pos) : result(result), pos(pos) {}
    ElemOfSparce() = default;

    T result;
    int64_t pos = 0;
  };

  SparseTable(const std::vector<T>& vec);

  T FindSec(int64_t left, int64_t right);

 private:
  ElemOfSparce FindMin(int64_t left, int64_t right);

  std::vector<std::vector<ElemOfSparce>> spar_;
  std::vector<int64_t> logs_;
  size_t size_;
};

template <typename T>
SparseTable<T>::SparseTable(const std::vector<T>& vec) {
  size_ = vec.size();
  spar_.resize(vec.size(),
               std::vector<ElemOfSparce>(ceil(std::log2l(vec.size()))));
  for (size_t i = 0; i < size_; ++i) {
    spar_[i][0] = ElemOfSparce(vec[i], i);
  }
  for (size_t j = 1; (1ULL << j) <= size_; ++j) {
    for (size_t i = 0; i + (1 << j) - 1 < size_; ++i) {
      if (spar_[i][j - 1].result <= spar_[i + (1 << (j - 1))][j - 1].result) {
        spar_[i][j] = spar_[i][j - 1];
      } else {
        spar_[i][j] = spar_[i + (1 << (j - 1))][j - 1];
      }
    }
  }
  logs_.resize(size_ + 1);
  logs_[1] = 0;
  for (size_t i = 2; i < size_ + 1; ++i) {
    logs_[i] = logs_[i / 2] + 1;
  }
}

template <typename T>
T SparseTable<T>::FindSec(int64_t left, int64_t right) {
  ElemOfSparce elem = FindMin(left, right);
  ElemOfSparce ans;
  ElemOfSparce left_elem;
  ElemOfSparce right_elem;

  if (elem.pos == left) {
    ans = FindMin(left + 1, right);
    return ans.result;
  }
  if (elem.pos == right) {
    ans = FindMin(left, right - 1);
    return ans.result;
  }

  left_elem = FindMin(left, elem.pos - 1);
  right_elem = FindMin(elem.pos + 1, right);
  if (left_elem.result < right_elem.result) {
    return left_elem.result;
  }
  return right_elem.result;
}

template <typename T>
typename SparseTable<T>::ElemOfSparce SparseTable<T>::FindMin(int64_t left,
                                                              int64_t right) {
  int64_t pos = logs_[right - left + 1];
  if (spar_[left][pos].result <= spar_[right - (1 << pos) + 1][pos].result) {
    return spar_[left][pos];
  }
  return spar_[right - (1 << pos) + 1][pos];
}