# Бинарная куча
В данной реализации поддерживается куча с произвольным типом и компаратором.
## Функции
1. Insert - вставляет в кучу элемент.
2. GetMin - выдает константную ссылку на минимум в куче.
3. ExtractMin - удаляет минимальный элемент из кучи.
4. DecreaseKey - уменьшает значение элемента, который был добавлен по запросу с номером key (предполагается, что этот элемент всё еще в куче), на значение value.
## P.S.
Подробнее про структуру можно прочитать [здесь](https://neerc.ifmo.ru/wiki/index.php?title=%D0%94%D0%B2%D0%BE%D0%B8%D1%87%D0%BD%D0%B0%D1%8F_%D0%BA%D1%83%D1%87%D0%B0).