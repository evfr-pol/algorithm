#include <iostream>
#include <string>
#include <vector>

template <typename T, typename Cmp = std::less<T>>
class Heap {
 public:
  Heap() = default;

  void Insert(const T& elem);

  const T& GetMin();

  void ExtractMin();

  void DecreaseKey(int64_t key, const T& value);

 private:
  size_t LeftChildInd(size_t index) { return 2ULL * index + 1ULL; }

  size_t RightChildInd(size_t index) { return 2ULL * index + 2ULL; }

  size_t ParentInd(size_t index) { return index > 0 ? (index - 1) / 2 : 0; }

  void SiftUp(size_t index);

  void SiftDown(size_t index);

  std::vector<std::pair<T, std::vector<int64_t>>> storage_;
  int64_t cur_req_ = 1;
  Cmp compar_ = std::less<T>();
};

template <typename T, typename Cmp>
void Heap<T, Cmp>::Insert(const T& elem) {
  storage_.push_back(std::make_pair(elem, std::vector<int64_t>(1, cur_req_)));
  SiftUp(storage_.size() - 1);
  ++cur_req_;
}

template <typename T, typename Cmp>
const T& Heap<T, Cmp>::GetMin() {
  storage_[0].second.push_back(cur_req_);
  ++cur_req_;
  return storage_[0].first;
}

template <typename T, typename Cmp>
void Heap<T, Cmp>::ExtractMin() {
  storage_[0] = storage_[storage_.size() - 1];
  storage_.pop_back();
  SiftDown(0);
  ++cur_req_;
}

template <typename T, typename Cmp>
void Heap<T, Cmp>::DecreaseKey(int64_t key, const T& value) {
  for (size_t i = 0; i < storage_.size(); ++i) {
    for (size_t j = 0; j < storage_[i].second.size(); ++j) {
      if (storage_[i].second[j] == key) {
        storage_[i].second.push_back(cur_req_);
        ++cur_req_;
        storage_[i].first -= value;
        SiftUp(i);
        break;
      }
    }
  }
}

template <typename T, typename Cmp>
void Heap<T, Cmp>::SiftUp(size_t index) {
  while (index > 0 &&
         compar_(storage_[index].first, storage_[(index - 1) / 2].first)) {
    std::swap(storage_[index], storage_[(index - 1) / 2]);
    index = ParentInd(index);
  }
}

template <typename T, typename Cmp>
void Heap<T, Cmp>::SiftDown(size_t index) {
  size_t left = LeftChildInd(index);
  size_t right = RightChildInd(index);
  while (left < storage_.size()) {
    size_t index_to_change = index;
    if (compar_(storage_[left].first, storage_[index].first)) {
      index_to_change = left;
    }
    if (right < storage_.size() &&
        compar_(storage_[right].first, storage_[index].first) &&
        compar_(storage_[right].first, storage_[left].first)) {
      index_to_change = right;
    }
    if (index_to_change == index) {
      break;
    }
    std::swap(storage_[index], storage_[index_to_change]);
    index = index_to_change;
    left = LeftChildInd(index);
    right = RightChildInd(index);
  }
}