#include <iostream>
#include <vector>

using Vec = std::vector<int64_t>;

Vec Merge(Vec first, Vec second) {
  size_t first_i = 0;
  size_t second_i = 0;
  Vec result;
  result.reserve(first.size() + second.size());

  while (first_i < first.size() && second_i < second.size()) {
    if (first[first_i] <= second[second_i]) {
      result.push_back(first[first_i]);
      ++first_i;
    } else {
      result.push_back(second[second_i]);
      ++second_i;
    }
  }

  while (first_i < first.size()) {
    result.push_back(first[first_i]);
    ++first_i;
  }
  while (second_i < second.size()) {
    result.push_back(second[second_i]);
    ++second_i;
  }

  return result;
}

Vec MergeSort(Vec vecc) {
  if (vecc.size() == 1) {
    return vecc;
  }
  Vec left_arg(vecc.size() / 2);
  Vec right_arg(vecc.size() - left_arg.size());
  for (size_t i = 0; i < left_arg.size(); ++i) {
    left_arg[i] = vecc[i];
  }
  for (size_t i = 0; i < right_arg.size(); ++i) {
    right_arg[i] = vecc[i + left_arg.size()];
  }
  Vec left = MergeSort(left_arg);
  Vec right = MergeSort(right_arg);
  Vec ans = Merge(left, right);
  return ans;
}

int main() {
  int32_t num;
  std::cin >> num;
  Vec mas(num);
  int64_t prom;
  for (int i = 0; i < num; ++i) {
    std::cin >> mas[i];
  }

  mas = MergeSort(mas);

  for (int i = 0; i < num; ++i) {
    std::cout << mas[i] << ' ';
  }
  return 0;
}