#include <iostream>
#include <vector>

namespace Const {
const uint64_t kByte = 8;
const uint64_t kSizeOfArr = 256;
const uint64_t kModul = 1 << 8;
}  // namespace Const

void LSD(std::vector<uint64_t>& vec, int64_t number_of_byte) {
  std::vector<uint64_t> count_bytes(Const::kSizeOfArr, 0);
  for (size_t i = 0; i < vec.size(); ++i) {
    ++count_bytes[(vec[i] >> number_of_byte) % Const::kModul];
  }

  std::vector<uint64_t> prev_sum(Const::kSizeOfArr, 0);
  prev_sum[0] = 0;
  for (size_t i = 1; i < prev_sum.size(); ++i) {
    prev_sum[i] = prev_sum[i - 1] + count_bytes[i - 1];
  }

  std::vector<uint64_t> result(vec.size(), 0);
  for (size_t i = 0; i < vec.size(); ++i) {
    result[prev_sum[(vec[i] >> number_of_byte) % Const::kModul]] = vec[i];
    ++prev_sum[(vec[i] >> number_of_byte) % Const::kModul];
  }

  size_t size = vec.size();
  vec = std::move(result);
  vec.resize(size);
}