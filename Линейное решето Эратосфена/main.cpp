#include <iostream>
#include <vector>

std::vector<int32_t> Eratosphen(int32_t num) {
  std::vector<int32_t> mind(num + 1);
  std::vector<int32_t> prim;
  for (int32_t i = 0; i < num + 1; ++i) {
    mind[i] = i;
  }
  for (int32_t i = 2; i < num + 1; ++i) {
    if (mind[i] == i) {
      prim.push_back(i);
    }
    for (auto& elem : prim) {
      if (elem * i >= (num + 1) || elem > mind[i]) {
        break;
      }
      mind[elem * i] = elem;
    }
  }
  return prim;
}

int main() {
  int32_t num;
  std::cin >> num;

  std::vector<int32_t> prim = Eratosphen(num);
  for (auto& elem : prim) {
    std::cout << elem << ' ';
  }
}
