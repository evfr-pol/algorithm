#include <algorithm>
#include <cmath>
#include <iostream>
#include <memory>
#include <vector>

namespace Const {
const int64_t kSize = std::pow(10, 9) + 1;
}  // namespace Const

template <typename T>
class Comparator {
 public:
  bool operator()(const std::pair<T, T>& beg, const std::pair<T, T>& end) {
    if (beg.first != end.first) {
      return beg.first < end.first;
    }
    return beg.second > end.second;
  }
};

template <typename T>
class Func {
 public:
  T operator()(const T& fir, const T& second) { return fir + second; }
};

template <typename T, template <typename> typename Func>
class SegmentTree {
 private:
  struct Node;

 public:
  SegmentTree(size_t size_of_arr) {
    par_ = std::make_unique<Node>(0, 4 * (size_of_arr_ - 1), 0);
    size_of_arr_ = size_of_arr;
  }

  T OperMode(std::unique_ptr<Node>& par, size_t gran_first,
             size_t gran_second) {
    if (gran_first > gran_second) {
      return T();
    }

    if (par->beg == gran_first && par->end == gran_second) {
      return par->sum;
    }

    size_t tmp = (par->beg + par->end) / 2;

    T left_res = T();
    if (par->left != nullptr) {
      left_res = OperMode(par->left, gran_first, std::min(tmp, gran_second));
    }
    T right_res = T();
    if (par->right != nullptr) {
      right_res =
          OperMode(par->right, std::max(gran_first, tmp + 1), gran_second);
    }

    return func_(left_res, right_res);
  }

  T Oper(size_t first, size_t second) { return OperMode(par_, first, second); }

  void UpdateMode(std::unique_ptr<Node>& par, size_t pos, const T& new_val) {
    if (par->beg == par->end) {
      par->sum = new_val;
    } else {
      size_t tmp = (par->beg + par->end) / 2;
      if (pos <= tmp) {
        if (par->left == nullptr) {
          par->left = std::make_unique<Node>(par->beg, tmp, 0);
        }
        UpdateMode(par->left, pos, new_val);
      } else {
        if (par->right == nullptr) {
          par->right = std::make_unique<Node>(tmp + 1, par->end, 0);
        }
        UpdateMode(par->right, pos, new_val);
      }

      T left_res = T();
      if (par->left != nullptr) {
        left_res = par->left->sum;
      }
      T right_res = T();
      if (par->right != nullptr) {
        right_res = par->right->sum;
      }
      par->sum = func_(left_res, right_res);
    }
  }

  void Update(size_t pos, const T& new_val) { UpdateMode(par_, pos, new_val); }

  size_t GetSize() { return size_of_arr_; }

 private:
  struct Node {
    Node(size_t beg, size_t end, T sum) : beg(beg), end(end), sum(sum) {}
    std::unique_ptr<Node> left = nullptr;
    std::unique_ptr<Node> right = nullptr;
    size_t beg;
    size_t end;
    T sum;
  };

  std::unique_ptr<Node> par_;
  Func<T> func_;
  size_t size_of_arr_;
};