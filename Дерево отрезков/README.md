# SegmentTreeOnVector
Здесь описано дерево отрезков с шаблонным типом, позволяющее изменять значение в точке, а также получать результат произвольной ассоциативной, коммутативной и имеющей нейтральный элемент функции на отрезке.
# SegmentTreeOnPtr
Здесь то же самое, что и в `SegmentTreeOnVector`, однако пока вершина не тронута, она в памяти не создаётся.
# P.S.
Подробнее об этих структурах можно прочитать [здесь](https://e-maxx.ru/algo/segment_tree).