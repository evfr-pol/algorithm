#include <iomanip>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

template <typename T>
class Func {
 public:
  T operator()(const T& fir, const T& second) { return fir + second; }
};

template <typename T, template <typename> typename Func>
class SegmentTree {
 public:
  SegmentTree(size_t size_of_arr) {
    vec_.resize(size_of_arr * 4ULL);
    size_of_arr_ = size_of_arr;
  }

  SegmentTree(const std::vector<T>& vec) {
    vec_.resize(vec.size() * 4ULL);
    Build(vec, 0, 0, vec.size() - 1);
    size_of_arr_ = vec.size();
  }

  T OperMode(size_t par, size_t left, size_t right,
             std::pair<size_t, size_t> gran) {
    if (gran.first > gran.second) {
      return T();
    }
    if (left == gran.first && right == gran.second) {
      return vec_[par];
    }
    size_t tmp = (left + right) / 2;
    return func_(
        OperMode(2 * par + 1, left, tmp,
                 std::make_pair(gran.first, std::min(tmp, gran.second))),
        OperMode(2 * par + 2, tmp + 1, right,
                 std::make_pair(std::max(gran.first, tmp + 1), gran.second)));
  }

  T Oper(size_t first, size_t second) {
    return OperMode(0, 0, size_of_arr_ - 1, std::make_pair(first, second));
  }

  void UpdateMode(size_t par, std::pair<size_t, size_t> gran, size_t pos,
                  const T& new_val) {
    if (gran.first == gran.second) {
      vec_[par] = new_val;
    } else {
      size_t tmp = (gran.first + gran.second) / 2;
      if (pos <= tmp) {
        UpdateMode(2 * par + 1, std::make_pair(gran.first, tmp), pos, new_val);
      } else {
        UpdateMode(2 * par + 2, std::make_pair(tmp + 1, gran.second), pos,
                   new_val);
      }
      vec_[par] = func_(vec_[2 * par + 1], vec_[2 * par + 2]);
    }
  }

  void Update(size_t pos, const T& new_val) {
    UpdateMode(0, std::make_pair(0, size_of_arr_ - 1), pos, new_val);
  }

  size_t GetSize() { return size_of_arr_; }

 private:
  void Build(const std::vector<T>& vec, size_t par, size_t left, size_t right) {
    if (left == right) {
      vec_[par] = vec[left];
    } else {
      size_t tmp = (left + right) / 2;
      Build(vec, 2 * par + 1, left, tmp);
      Build(vec, 2 * par + 2, tmp + 1, right);
      vec_[par] = func_(vec_[2 * par + 1], vec_[2 * par + 2]);
    }
  }
  std::vector<T> vec_;
  Func<T> func_;
  size_t size_of_arr_;
};