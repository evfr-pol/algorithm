# QuickSort O(nlogn)
Этот алгоритм - детерминированный алгоритм QuickSort, что достигается методом медианы медиан, также этот алгоритм ускорен тем, что `Partition` делит массив на 3 части: меньше `pivot`, равно `pivot`, больше `pivot`.
# KthStatistics O(n)
Работает аналогично QuickSort.
Подробнее про эти алгоритмы можно прочитать [здесь](https://neerc.ifmo.ru/wiki/index.php?title=%D0%91%D1%8B%D1%81%D1%82%D1%80%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0).