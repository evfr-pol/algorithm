#include <iostream>
#include <vector>

namespace Const {
const size_t kModul = 5;
}

void BubleSort(size_t left, size_t right, std::vector<int64_t>& vec) {
  if (left >= right) {
    return;
  }

  for (int64_t i = 0; i <= static_cast<int64_t>(right - left); ++i) {
    for (int64_t j = static_cast<int64_t>(left);
         j <= static_cast<int64_t>(right) - i - 1; ++j) {
      if (vec[j] > vec[j + 1]) {
        std::swap(vec[j], vec[j + 1]);
      }
    }
  }
}

int64_t ChoosePivot(size_t left, size_t right, std::vector<int64_t>& vec) {
  size_t left_copy = left;
  if (right - left + 1 <= 2 * Const::kModul) {
    BubleSort(left, right, vec);
    return vec[left + (right - left) / 2];
  }

  std::vector<int64_t> copy_vec;
  while (left_copy + Const::kModul - 1 <= right) {
    BubleSort(left_copy, left_copy + Const::kModul - 1, vec);
    copy_vec.push_back(vec[left_copy + 2]);
    left_copy += Const::kModul;
  }

  if (left_copy <= right) {
    BubleSort(left_copy, right, vec);
    copy_vec.push_back(vec[left_copy + (right - left_copy) / 2]);
  }

  return ChoosePivot(0, copy_vec.size() - 1, copy_vec);
}  // Median of Medians

std::pair<size_t, size_t> Partition(size_t left, size_t right,
                                    std::vector<int64_t>& vec) {
  // std::swap(vec[left], vec[left + (right - left) / 2]);
  int64_t pivot = ChoosePivot(left, right, vec);
  size_t left_ind = left;
  while ((left_ind <= right) && (vec[left_ind] < pivot)) {
    ++left_ind;
  }

  size_t ind_to_change = 0;
  for (size_t i = left_ind + 1; i <= right; ++i) {
    if (vec[i] < pivot) {
      std::swap(vec[left_ind + ind_to_change], vec[i]);
      ++ind_to_change;
    }
  }

  size_t ind_part = 0;
  for (size_t i = left_ind + ind_to_change; i <= right; ++i) {
    if (vec[i] == pivot) {
      std::swap(vec[left_ind + ind_to_change + ind_part], vec[i]);
      ++ind_part;
    }
  }

  return std::make_pair(left_ind + ind_to_change,
                        left_ind + ind_to_change + ind_part);
}

void QuickSort(size_t left, size_t right, std::vector<int64_t>& vec) {
  if (left >= right) {
    return;
  }
  if (right - left + 1 <= 2 * Const::kModul) {
    BubleSort(left, right, vec);
  } else {
    auto boarder = Partition(left, right, vec);
    QuickSort(boarder.second, right, vec);
    if (boarder.first != left) {
      QuickSort(left, boarder.first - 1, vec);
    }
  }
}