#include <cmath>
#include <iostream>
#include <vector>

void BubleSort(size_t left, size_t right, std::vector<uint32_t>& vec) {
  if (left >= right) {
    return;
  }
  for (int i = 0; i <= static_cast<int>(right - left); ++i) {
    for (int j = static_cast<int>(left); j <= static_cast<int>(right) - i - 1;
         ++j) {
      if (vec[j] > vec[j + 1]) {
        std::swap(vec[j], vec[j + 1]);
      }
    }
  }
}

uint32_t ChoosePivot(size_t left, size_t right, std::vector<uint32_t>& vec) {
  size_t left_copy = left;
  if (right - left + 1 <= 2 * Const::kModul) {
    BubleSort(left, right, vec);
    return vec[left + (right - left) / 2];
  }

  std::vector<uint32_t> copy_vec;
  while (left_copy + Const::kModul - 1 <= right) {
    BubleSort(left_copy, left_copy + Const::kModul - 1, vec);
    copy_vec.push_back(vec[left_copy + 2]);
    left_copy += Const::kModul;
  }

  if (left_copy <= right) {
    BubleSort(left_copy, right, vec);
    copy_vec.push_back(vec[left_copy + (right - left_copy) / 2]);
  }

  return ChoosePivot(0, copy_vec.size() - 1, copy_vec);
}  // Median of Medians

std::pair<size_t, size_t> Partition(size_t left, size_t right,
                                    std::vector<uint32_t>& vec) {
  // std::swap(vec[left], vec[left + (right - left) / 2]);
  uint32_t pivot = ChoosePivot(left, right, vec);

  size_t left_ind = left;
  while ((left_ind <= right) && (vec[left_ind] < pivot)) {
    ++left_ind;
  }

  size_t ind_to_change = 0;
  for (size_t i = left_ind + 1; i <= right; ++i) {
    if (vec[i] < pivot) {
      std::swap(vec[left_ind + ind_to_change], vec[i]);
      ++ind_to_change;
    }
  }

  size_t ind_part = 0;
  for (size_t i = left_ind + ind_to_change; i <= right; ++i) {
    if (vec[i] == pivot) {
      std::swap(vec[left_ind + ind_to_change + ind_part], vec[i]);
      ++ind_part;
    }
  }

  return {left_ind + ind_to_change, left_ind + ind_to_change + ind_part};
}

uint32_t FindKth(std::vector<uint32_t>& vec, size_t left, size_t right,
                 size_t kth_stat) {
  auto boarder = Partition(left, right, vec);
  if (boarder.second == right + 1 ||
      vec[boarder.second] != vec[boarder.first]) {
    --boarder.second;
  }
  if (left + kth_stat - 1 >= boarder.first &&
      left + kth_stat - 1 <= boarder.second) {
    return vec[boarder.first];
  }
  if (left + kth_stat - 1 < boarder.first) {
    return FindKth(vec, left, boarder.first - 1, kth_stat);
  }
  return FindKth(vec, boarder.second + 1, right,
                 kth_stat - (boarder.second - left + 1));
}