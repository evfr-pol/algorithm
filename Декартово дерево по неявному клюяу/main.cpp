#include <ctime>
#include <iostream>
#include <memory>
#include <random>
#include <string>
#include <unordered_set>

namespace Const {
const int64_t kMaxVal = 1e9 + 2;
}  // namespace Const

class Treap {
 private:
  struct Node {
    Node(int64_t value, int64_t prior)
        : value(value),
          prior(prior),
          cnt(1),
          min_in_tree(value),
          reverse(false),
          left(nullptr),
          right(nullptr) {}

    int64_t value;
    int64_t prior;
    int64_t cnt;
    int64_t min_in_tree;
    bool reverse;
    Node* left;
    Node* right;
  };

 public:
  Treap() : root_(nullptr), rando_(std::time(0)) {}

 private:
  static int64_t Cnt(Node* treap) {
    if (treap == nullptr) {
      return 0;
    }
    return treap->cnt;
  }

  static void UpdateCnt(Node* treap) {
    if (treap != nullptr) {
      treap->cnt = Cnt(treap->left) + Cnt(treap->right) + 1;
    }
  }

  static int64_t Min(Node* treap) {
    if (treap == nullptr) {
      return Const::kMaxVal;
    }
    return treap->min_in_tree;
  }

  static void UpdateMin(Node* treap) {
    if (treap != nullptr) {
      treap->min_in_tree =
          std::min(std::min(Min(treap->left), Min(treap->right)), treap->value);
    }
  }

  static void Prot(Node* treap) {
    if (treap != nullptr && treap->reverse) {
      treap->reverse = false;
      std::swap(treap->left, treap->right);
      if (treap->left != nullptr) {
        treap->left->reverse = !treap->left->reverse;
      }
      if (treap->right != nullptr) {
        treap->right->reverse = !treap->right->reverse;
      }
    }
  }

  void Split(Node* treap, int64_t key, int64_t plus, Node*& left,
             Node*& right) {
    if (key < 0) {
      left = nullptr;
      right = treap;
      return;
    }
    if (treap == nullptr) {
      left = nullptr;
      right = nullptr;
      return;
    }
    Prot(treap);
    int64_t cur_key = plus + Cnt(treap->left);
    if (key <= cur_key) {
      Split(treap->left, key, plus, left, treap->left);
      right = treap;
    } else {
      Split(treap->right, key, cur_key + 1, treap->right, right);
      left = treap;
    }
    UpdateCnt(treap);
    UpdateMin(treap);
  }  // split по элементу оставляет cur_key в правом поддереве

  void Merge(Node*& treap, Node* left, Node* right) {
    Prot(left);
    Prot(right);
    if (left == nullptr || right == nullptr) {
      treap = ((left == nullptr) ? right : left);
      return;
    }
    if (left->prior > right->prior) {
      Merge(left->right, left->right, right);
      treap = left;
    } else {
      Merge(right->left, left, right->left);
      treap = right;
    }
    UpdateCnt(treap);
    UpdateMin(treap);
  }

 public:
  void Reverse(int64_t left, int64_t right) {
    Node* treap1;
    Node* treap2;
    Node* treap3;
    Split(root_, left, 0, treap1, treap2);
    Split(treap2, right - left + 1, 0, treap2, treap3);
    if (treap2 == nullptr) {
      return;
    }
    treap2->reverse = !treap2->reverse;
    Merge(root_, treap1, treap2);
    Merge(root_, root_, treap3);
  }

  void Insert(int64_t pos, int64_t value) {
    int64_t priority = rando_();

    Node* right = new Node(value, priority);
    Node* treap1;
    Node* treap2;

    Split(root_, pos, 0, treap1, treap2);
    Merge(treap1, treap1, right);
    Merge(root_, treap1, treap2);
  }

  int64_t GetMin(int64_t left, int64_t right) {
    Node* treap1;
    Node* treap2;
    Node* treap3;

    Split(root_, left, 0, treap1, treap2);
    Split(treap2, right - left + 1, 0, treap2, treap3);

    int64_t ans = Min(treap2);

    Merge(root_, treap1, treap2);
    Merge(root_, root_, treap3);

    return ans;
  }

  void Delete(Node* ptr) {
    if (ptr == nullptr) {
      return;
    }
    Delete(ptr->left);
    Delete(ptr->right);
    delete ptr;
  }

  ~Treap() { Delete(root_); }

 private:
  Node* root_ = nullptr;
  // std::unordered_set<int64_t> priority_;
  std::mt19937_64 rando_;
};