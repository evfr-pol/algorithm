#include <iostream>
#include <vector>

struct Coord {
  int64_t i_coord;
  int64_t j_coord;
  int64_t k_coord;
  Coord(int64_t i_coord, int64_t j_coord, int64_t k_coord)
      : i_coord(i_coord), j_coord(j_coord), k_coord(k_coord) {}
};

template <typename T>
class Fenwik {
 public:
  Fenwik(int64_t i_size, int64_t j_size, int64_t k_size)
      : vec_(i_size,
             std::vector<std::vector<T>>(j_size, std::vector<T>(k_size))),
        base_coord_(i_size, j_size, k_size) {}

  T FindSum(const Coord& fir_co, const Coord& sec_co);

  void Plus(const Coord& coord, const T& delta);

 private:
  T FindPrefSum(const Coord& coord);

  std::vector<std::vector<std::vector<T>>> vec_;
  Coord base_coord_;
};

template <typename T>
T Fenwik<T>::FindSum(const Coord& fir_co, const Coord& sec_co) {
  T bb_f = FindPrefSum(Coord(fir_co.i_coord - 1, sec_co.j_coord,
                             sec_co.k_coord));  // big block first
  T bb_s = FindPrefSum(Coord(sec_co.i_coord, fir_co.j_coord - 1,
                             sec_co.k_coord));  // big block second
  T bb_t = FindPrefSum(Coord(sec_co.i_coord, sec_co.j_coord,
                             fir_co.k_coord - 1));  // big block third
  T sb_f = FindPrefSum(Coord(fir_co.i_coord - 1, fir_co.j_coord - 1,
                             sec_co.k_coord));  // small block first
  T sb_s = FindPrefSum(Coord(fir_co.i_coord - 1, sec_co.j_coord,
                             fir_co.k_coord - 1));  // small block second
  T sb_t = FindPrefSum(Coord(sec_co.i_coord, fir_co.j_coord - 1,
                             fir_co.k_coord - 1));  // small block third
  T result = FindPrefSum(sec_co) - bb_f - bb_s - bb_t + sb_f + sb_s + sb_t -
             FindPrefSum(Coord(fir_co.i_coord - 1, fir_co.j_coord - 1,
                               fir_co.k_coord - 1));
  return result;
}

template <typename T>
void Fenwik<T>::Plus(const Coord& coord, const T& delta) {
  for (int64_t i = coord.i_coord; i < base_coord_.i_coord; i = (i | (i + 1))) {
    for (int64_t j = coord.j_coord; j < base_coord_.j_coord;
         j = (j | (j + 1))) {
      for (int64_t k = coord.k_coord; k < base_coord_.k_coord;
           k = (k | (k + 1))) {
        vec_[i][j][k] += delta;
      }
    }
  }
}

template <typename T>
T Fenwik<T>::FindPrefSum(const Coord& coord) {
  if (coord.i_coord < 0 || coord.j_coord < 0 || coord.k_coord < 0) {
    return T();
  }
  T result = T();
  for (int64_t i = coord.i_coord; i >= 0; i = (i & (i + 1)) - 1) {
    for (int64_t j = coord.j_coord; j >= 0; j = (j & (j + 1)) - 1) {
      for (int64_t k = coord.k_coord; k >= 0; k = (k & (k + 1)) - 1) {
        result += vec_[i][j][k];
      }
    }
  }
  return result;
}